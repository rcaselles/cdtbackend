const express = require("express");
const app = express();
const configuration = require("./src/config/config");
const corsConfig = require("./src/config/cors-config");
const { get, post } = require("./src/index");
const bodyParser = require("body-parser");
//Enabling body parsing
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
//Cors Protection
app.use(corsConfig.cors);
//===================POST
//Login
app.post("/login", post.Login);
//Users
app.post("/users/update/:id?", post.updateUsers);
app.post("/users/password/:id?", post.updatePassword);

//===================GET
//Resources
app.get("/resources/:sport/:id?", get.getResources);
//Users
app.get("/users/:id?", get.getUsers);
//Reservations
app.get("/reservations", get.getReservations);
//Announcements
app.get("/announcements/", get.getAnnouncement);
app.get("/announcements/latest", get.getLatestAnnouncement);
//Layouts
app.get("/layouts/:id", get.getLayouts);

app.listen(configuration.port, () => {
  console.log("***************************************");
  console.log("Club De Tenis Denia Backend v" + configuration.version);
  console.log(
    "El servidor está inicializado en el puerto " + configuration.port
  );
});
