const configuration = require("./config");
const mysql = require("mysql");

const pool = mysql.createPool(configuration.dbconfig);
module.exports = pool;
