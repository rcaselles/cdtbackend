const cors = require("cors");

const allowedOrigins = [
  "http://localhost:3000",
  "http://localhost:1234",
  "http://192.168.0.11:1234",
  "http://192.168.0.11:3000",
];
const corsConfig = cors({
  origin: function (origin, callback) {
    if (!origin) return callback(null, true);
    if (allowedOrigins.indexOf(origin) === -1) {
      var msg =
        "The CORS policy for this site does not " +
        "allow access from the specified Origin.";
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  },
});

module.exports = { cors: corsConfig };
