//GET
const getResources = require("./controllers/resources/resources");
const getUsers = require("./controllers/users/users");
const getReservations = require("./controllers/reservations/reservations");
const getLatestAnnouncement = require("./controllers/announcements/latestannouncement");
const getAnnouncement = require("./controllers/announcements/announcement");
const getLayouts = require("./controllers/layouts/layouts");
//POST
const Login = require("./controllers/login/login");
const updateUsers = require("./controllers/updateuser/updateuser");
const updatePassword = require("./controllers/updatepassword/updatePassword");
module.exports = {
  get: {
    getResources,
    getUsers,
    getReservations,
    getLatestAnnouncement,
    getLayouts,
    getAnnouncement,
  },
  post: {
    Login,
    updateUsers,
    updatePassword,
  },
  delete: {},
};
