const jwt = require("jsonwebtoken");
const pool = require("../../config/db-config");
const configuration = require("../../config/config");

const getLayouts = (req, res) => {
  const token = req.headers["access-token"];
  if (token) {
    jwt.verify(token, configuration.secret, (err, decoded) => {
      if (err) {
        res.status(401).send({ status: "error", id: "5" });
      } else {
        pool.query(
          "SELECT * FROM time_blocks WHERE layout_id = (SELECT layout_id from schedules WHERE schedule_id = " +
            req.params.id +
            ")",
          (error, result) => {
            if (error)
              res
                .status(401)
                .send({ status: "error", id: "6", content: error });
            res.send(result);
          }
        );
      }
    });
  } else {
    res.status(401).send({ status: "error", id: "4" });
  }
};
module.exports = getLayouts;
