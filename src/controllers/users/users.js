const jwt = require("jsonwebtoken");
const pool = require("../../config/db-config");
const configuration = require("../../config/config");

const getUsers = (req, res) => {
  const token = req.headers["access-token"];
  if (token) {
    jwt.verify(token, configuration.secret, (err, decoded) => {
      if (err) {
        res.status(401).send({ status: "error", id: "5" });
      } else {
        const id = req.params.id;
        //Todos los usuarios (Solo administrador)
        if (id === undefined && decoded.isadmin == true) {
          pool.query(
            "SELECT users.*, user_groups.group_id,custom_attribute_values.attribute_value AS sunpad FROM users LEFT JOIN user_groups ON (user_groups.user_id = users.user_id) LEFT JOIN custom_attribute_values ON (custom_attribute_values.entity_id = users.user_id) WHERE custom_attribute_values.attribute_category = 2;",
            (error, result) => {
              if (error) {
                res
                  .status(401)
                  .send({ status: "error", reason: error.sqlMessage });
              } else {
                if (result.length > 0) {
                  res.send(result);
                } else {
                  res
                    .status(401)
                    .send({ status: "error", reason: "No users in database" });
                }
              }
            }
          );
        } else if (decoded.isadmin == true && id.toString() != "logged") {
          //Parte administrador
          pool.query(
            "SELECT users.*, user_groups.group_id,custom_attribute_values.attribute_value AS sunpad FROM users LEFT JOIN user_groups ON (user_groups.user_id = users.user_id) LEFT JOIN custom_attribute_values ON (custom_attribute_values.entity_id = users.user_id) WHERE custom_attribute_values.attribute_category = 2 AND users.user_id = ?;",
            id,
            (error, result) => {
              if (error) {
                res
                  .status(401)
                  .send({ status: "error", reason: "error.sqlMessage" });
              } else {
                if (result.length > 0) {
                  res.send(result);
                } else {
                  res.status(401).send({
                    status: "error",
                    reason: "There is no user with that id",
                  });
                }
              }
            }
          );
        } else {
          //Parte usuarios
          pool.query(
            "SELECT users.*, user_groups.group_id,custom_attribute_values.attribute_value AS sunpad FROM users LEFT JOIN user_groups ON (user_groups.user_id = users.user_id) LEFT JOIN custom_attribute_values ON (custom_attribute_values.entity_id = users.user_id) WHERE custom_attribute_values.attribute_category = 2 AND users.user_id = ?;",
            decoded.userid,
            (error, result) => {
              if (error) {
                res
                  .status(401)
                  .send({ status: "error", reason: "error.sqlMessage" });
              } else {
                result[0].isadmin = result[0].group_id === 2;
                res.send(result);
              }
            }
          );
        }
      }
    });
  } else {
    res.status(401).send({ status: "error", id: "4" });
  }
};
module.exports = getUsers;
