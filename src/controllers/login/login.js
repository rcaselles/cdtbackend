const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const configuration = require("../../config/config");
const pool = require("../../config/db-config");

const Login = (req, res) => {
  const user = req.query.user;
  const password = req.query.password;
  if (user === undefined || password === undefined) {
    res
      .status(401)
      .send({ status: "error", error: "Hay un error con tu petición" });
  } else {
    pool.query(
      'SELECT * FROM users INNER JOIN user_groups ON (user_groups.user_id = users.user_id) WHERE users.username = "' +
        user +
        '" OR users.email = "' +
        user +
        '" LIMIT 1',
      (error, result) => {
        if (result.length == 1) {
          let salt = result[0].salt;
          let passwordtoenc = password + salt;

          var shasum = crypto.createHash("sha1");
          shasum.update(passwordtoenc);
          let passworddef = shasum.digest("hex");
          if (passworddef == result[0].password) {
            let payload = {
              userid: result[0].user_id,
              isadmin: result[0].group_id == 2 ? true : false,
            };
            let jwttoken = jwt.sign(payload, configuration.secret, {
              expiresIn: "1d",
            });

            let response = {
              status: "ok",
              userid: result[0].user_id,
              isadmin: result[0].group_id == 2 ? true : false,
              token: jwttoken,
            };
            res.send(response);
          } else {
            let response = {
              status: "error",
              error: "La contraseña no es correcta",
            };
            res.send(response);
          }
        } else {
          res.status(401).send({
            status: "error",
            error: "No hay un usuario con esas credenciales",
          });
        }
      }
    );
  }
};
module.exports = Login;
