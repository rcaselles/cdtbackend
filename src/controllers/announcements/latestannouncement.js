const jwt = require("jsonwebtoken");
const configuration = require("../../config/config");
const pool = require("../../config/db-config");

const getLatestAnnouncement = (req, res) => {
  const token = req.headers["access-token"];
  if (token) {
    jwt.verify(token, configuration.secret, (err, decoded) => {
      if (err) {
        res.status(401).send({ status: "error", id: "5" });
      } else {
        pool.query(
          "SELECT * FROM announcements WHERE enabled = 1 and start_date = (SELECT MAX(start_date) FROM announcements WHERE enabled = 1) LIMIT 1",
          (error, result) => {
            if (error) {
              return res
                .status(400)
                .send({ status: "error", id: "6", content: error });
            } else {
              res.send(result[0]);
            }
          }
        );
      }
    });
  } else {
    res.status(400).send({ status: "error", id: "4" });
  }
};
module.exports = getLatestAnnouncement;
