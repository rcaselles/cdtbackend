const jwt = require("jsonwebtoken");
const pool = require("../../config/db-config");
const configuration = require("../../config/config");

const getReservations = (req, res) => {
  const token = req.headers["access-token"];
  if (token) {
    jwt.verify(token, configuration.secret, (err, decoded) => {
      if (err) {
        res.status(401).send({ status: "error", id: "5" });
      } else {
        //Comprobando la start date debería sobrar+
        var fecha = req.query.date;
        var date = new Date(fecha);
        var currentWeekDay = date.getDay();
        var lessDays = currentWeekDay == 0 ? 6 : currentWeekDay - 1;
        //Obtenemos principio y fin de la semana dada
        var wkStart = new Date(
          new Date(date).setDate(date.getDate() - lessDays)
        );
        var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate() + 6));
        const resource_id = req.query.rid;

        pool.query(
          "SELECT * FROM `reservation_instances` ri INNER JOIN `reservation_resources` rr ON rr.series_id = ri.series_id" +
            " INNER JOIN `reservation_series` rs ON rs.series_id = ri.series_id INNER JOIN `users` us ON rs.owner_id = us.user_id WHERE ri.start_date BETWEEN '" +
            wkStart.toISOString().substring(0, 10) +
            "'" +
            "AND '" +
            wkEnd.toISOString().substring(0, 10) +
            " 23:59' AND rr.resource_id = " +
            resource_id +
            " AND rs.status_id = 1;",
          (error, result) => {
            if (error)
              res
                .status(401)
                .send({ status: "error", id: "6", content: error });
            let resultAux = result;
            for (var i = 0; i < resultAux.length; i++) {
              //First date - Start date
              let date = new Date(resultAux[i].start_date);
              date.setHours(new Date(resultAux[i].start_date).getHours() + 2);
              resultAux[i].start_date = date.toLocaleString("es-ES", {
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit",
              });
              //Second date - End date
              let date2 = new Date(resultAux[i].end_date);
              date2.setHours(new Date(resultAux[i].end_date).getHours() + 2);
              resultAux[i].end_date = date2.toLocaleString("es-ES", {
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit",
              });
              //Third date - Date of creation
              let date3 = new Date(resultAux[i].date_created);
              date3.setHours(
                new Date(resultAux[i].date_created).getHours() + 2
              );
              resultAux[i].date_created = date3.toLocaleString("es-ES", {
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit",
              });
              //probably here unsetting private data if user is not admin
            }
            res.send(resultAux);
          }
        );
      }
    });
  } else {
    res.status(401).send({ status: "error", id: "4" });
  }
};
module.exports = getReservations;
