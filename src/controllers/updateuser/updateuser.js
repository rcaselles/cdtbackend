const jwt = require("jsonwebtoken");
const pool = require("../../config/db-config");
const configuration = require("../../config/config");

const updateUsers = (req, res) => {
  const token = req.headers["access-token"];
  if (token) {
    jwt.verify(token, configuration.secret, (err, decoded) => {
      if (err) {
        return res
          .status(401)
          .send({ status: "error", content: "Unauthorized" });
      } else {
        let id = req.params.id;
        if (id == undefined) {
          id = decoded.userid;
          const email = req.body.email;
          const username = req.body.username;
          const fname = req.body.fname;
          const lname = req.body.lname;
          const phone = req.body.phone;
          const company = req.body.company;
          pool.query(
            "UPDATE users SET email = '" +
              email +
              "', username = '" +
              username +
              "', fname = '" +
              fname +
              "', lname = '" +
              lname +
              "', phone = '" +
              phone +
              "', organization = '" +
              company +
              "' WHERE user_id = " +
              id,
            (error, result) => {
              if (error) {
                return res
                  .status(401)
                  .send({ status: "error", content: "Unauthorized" });
              } else {
                return res.status(200).send();
              }
            }
          );
        } else {
          //TODO: Check if user is admin and start advanced mode
        }
      }
    });
  } else {
    return res.status(401).send({ status: "error", id: "4" });
  }
};
module.exports = updateUsers;
