const jwt = require("jsonwebtoken");
const pool = require("../../config/db-config");
const configuration = require("../../config/config");
const crypto = require("crypto");

const updatePassword = (req, res) => {
  const token = req.headers["access-token"];
  if (token) {
    jwt.verify(token, configuration.secret, (err, decoded) => {
      if (err) {
        return res
          .status(401)
          .send({ status: "error", content: "Unauthorized" });
      } else {
        let id = req.params.id;
        if (id == undefined) {
          id = decoded.userid;
          const oldpassword = req.body.oldpassword;
          const newpassword = req.body.password;

          pool.query(
            "SELECT * FROM users WHERE user_id = '" + id + "'",
            (error, result) => {
              if (error) {
                return res
                  .status(401)
                  .send({ status: "error", content: "Unauthorized" });
              } else {
                let passwordtoenc = oldpassword + result[0].salt;
                let shasum = crypto.createHash("sha1");
                shasum.update(passwordtoenc);
                let passworddef = shasum.digest("hex");
                if (passworddef != result[0].password) {
                  return res
                    .status(401)
                    .send({ status: "error", content: "Unauthorized" });
                } else {
                  //! We update password
                  let shasum2 = crypto.createHash("sha1");
                  let newpasswordA = newpassword + result[0].salt;
                  shasum2.update(newpasswordA);
                  let defNewPassword = shasum2.digest("hex");
                  pool.query(
                    "UPDATE users SET password = '" +
                      defNewPassword +
                      "' WHERE user_id = '" +
                      id +
                      "'",
                    (error, result) => {
                      if (error) {
                        return res
                          .status(401)
                          .send({ status: "error", content: "Unauthorized" });
                      } else {
                        return res.status(200).send();
                      }
                    }
                  );
                }
                return res.status(200).send(result[0].password);
              }
            }
          );
        } else {
          //TODO: Check if user is admin and start advanced mode
          return res.status(200).send();
        }
      }
    });
  } else {
    return res.status(401).send({ status: "error", id: "4" });
  }
};
module.exports = updatePassword;
