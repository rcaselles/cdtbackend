const jwt = require("jsonwebtoken");
const pool = require("../../config/db-config");
const configuration = require("../../config/config");

const getResources = (req, res) => {
  const token = req.headers["access-token"];
  const id = req.params.id;

  if (token) {
    jwt.verify(token, configuration.secret, (err, decoded) => {
      if (err) {
        res.status(500).send({ status: "error", reason: "unverified request" });
      } else {
        const sport = req.params.sport;
        if (id == undefined) {
          pool.query(
            "SELECT * FROM resources WHERE sport = '" +
              sport +
              "' ORDER BY name;",
            (err, result) => {
              if (err) {
                res
                  .status(500)
                  .send({ status: "error", reason: "error with request" });
              } else {
                res.send(result);
              }
            }
          );
        } else {
          pool.query(
            "SELECT * FROM resources WHERE sport = '" +
              sport +
              "' and resource_id = '" +
              id +
              "'ORDER BY name;",
            (err, result) => {
              if (err) {
                res
                  .status(500)
                  .send({ status: "error", reason: "error with request" });
              } else {
                res.send(result[0]);
              }
            }
          );
        }
      }
    });
  }
};

module.exports = getResources;
